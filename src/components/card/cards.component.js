import React from 'react';
import getPrice from '../../helpers/coindesk';
import Math from '../../helpers/math'
import './card.component.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment'



class Card extends React.Component {

    constructor(prop) {
        super(prop)
        this.state = {
            renderedCards: 0,
            state_cards: [],
            start_date: '',
            end_date: '',
        }
        console.log(prop)
        this.Submit = this.Submit.bind(this)

    this.handleStart = this.handleStart.bind(this);
    this.handleEnd = this.handleEnd.bind(this);
    this.Submit = this.Submit.bind(this)
}

    handleStart(date) {
        this.setState({start_date: date});
    }

    handleEnd(date) {
        this.setState({end_date: date});
    }

    Submit() {
        var d1 = new Date(this.state.start_date);
	    var d2 = new Date(this.state.end_date);
	    var timeDiff = d2.getTime() - d1.getTime();
        var DaysDiff = timeDiff / (1000 * 3600 * 24);
        if (DaysDiff < 182) {
            this.CoinDesk(this.state.start_date, this.state.end_date)
        } else {
            alert('Only Six Months Allowed')
        }
    }

    getDate(date) {
        let value = "";
        console.log(date);
        value = value.concat(`${date.getFullYear()}/`);
        value = value.concat(`${date.getMonth() + 1}/`);
        value = value.concat(`${date.getDate()}`);
        return value;
    }
    

    CoinDesk() {
        let values = []
        var self = this;
        const start = moment(this.state.start_date).format("YYYY-MM-DD");
        const end = moment(this.state.end_date).format("YYYY-MM-DD");
        getPrice(start, end).then((result) => {
            Object.entries(result).forEach(element => {
                let header = element[0]
                values.push([result[element[0]], header]);
            });
            self.setState({
                state_cards: values 
            })
        }).catch((err) => {
            console.log(err)
        });
    }

    getCard(value, name) {
        const green = "#1ABA6A"
        const red = "#D33F49"
        return (
            <div className="card-container" style={{backgroundColor: (value ? green : red)}}>
                <h1>{name}</h1>
                <div className="content" key={this.state.renderedCards}>
                    <h2>Lenght of prime is <br></br>{value ? "A Prime Number" : "Is not a prime number"}</h2>
                </div>
            </div>
        )
    }

    render() { 
        const math = new Math();
        return (
            <div className="container">
                <div className="date-container">
              
                <DatePicker
                    className="input"
                    selected={this.state.start_date}
                    onChange={this.handleStart}
                />

                <DatePicker
                    className="input"
                    selected={this.state.end_date}
                    onChange={this.handleEnd}
                />
                <input className="Submit" type="submit" value="Submit" onClick={this.Submit}/>
                </div>
                {
                    this.state.state_cards.map((result) => {
                        let isPrime = math.isPrime(Object.entries(math.Prime(result[0])).length);
                        return this.getCard(isPrime, result[1]);
                    })
                }
            </div>
        )
    }
}

export default Card;