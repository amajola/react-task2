async function getPrice(start_date, end_date) {
    let Prices = []
    const url = `https://api.coindesk.com/v1/bpi/historical/close.json?start=${start_date}&end=${end_date}`
    await fetch(url).then(result => {
         return result.json()
    }).then(result => {
        Prices = result.bpi
    });
    return (Prices);
}


export default getPrice;