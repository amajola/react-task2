
class Math {

    isPrime(number) {
        if (number === 1)
            return false
        for (let x = 2; x < number; x++) {
            if (number % x === 0) {
                return false
            }
        } return true;
    }
    
    Prime(number) {
        let primeArr = [];
        let counter = 1;
        while (counter <= number) {
            if (this.isPrime(counter) === true) {
                primeArr.push(counter)
            }
            counter++;
        }
        return (primeArr)
    
    }
}

export default Math;

